package com.example.testtask.fragments


import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.time.LocalDate
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testtask.R
import com.example.testtask.adapter.MultiTypeRecyclerViewAdapter
import com.example.testtask.databinding.FragmentBlockBinding
import com.example.testtask.model.DateCell
import com.example.testtask.model.TaskCell
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*


class BlockFragment : Fragment() {
    lateinit var bindingClass: FragmentBlockBinding
    private val adapter = MultiTypeRecyclerViewAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        bindingClass = FragmentBlockBinding.inflate(inflater,container,false)
        return bindingClass.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

            bindingClass.apply {

                rcViewFragmentBlock.layoutManager = LinearLayoutManager(activity)
                rcViewFragmentBlock.adapter = adapter
                val item1 = DateCell(
                    1,
                    dayOfWeek(),
                    getDayOfMonth(),
                    getMonth()
                )
                val item2 = TaskCell(
                    2,
                    "11:15",
                    "12:15",
                    "Йога",
                    "5",
                    "Большой зал",
                    R.drawable.background_green,
                    R.drawable.ic_people_two
                )
                val item3 = TaskCell(
                    2,
                    "15:15",
                    "16:15",
                    "Силовая тренировка",
                    "Иванов Дмитрий",
                    "Тренажерный зал",
                    R.drawable.background_orange,
                    R.drawable.ic_person_one
                )
                val item4 = DateCell(
                    1,
                    getTomorrow(1),
                    getTomorrowDateOfTheMonth(1),
                    getTomorrowMonth(1)
                )
                val item5 = TaskCell(
                    2,
                    "12:15",
                    "13:15",
                    "Легкая атлетика",
                    "Кукурузин Иван",
                    "Тренажерный зал",
                    R.drawable.background_orange,
                    R.drawable.ic_person_one
                )
                val item6 = TaskCell(
                    2,
                    "15:15",
                    "16:15",
                    "Кросфит",
                    "Мухнин Артём",
                    "Тренажерный зал",
                    R.drawable.background_orange,
                    R.drawable.ic_person_one
                )
                val item7 = TaskCell(
                    2,
                    "19:15",
                    "20:15",
                    "Йога",
                    "9",
                    "Большой зал",
                    R.drawable.background_green,
                    R.drawable.ic_people_two
                )
                val item8 = DateCell(
                    1,
                    getTomorrow(2),
                    getTomorrowDateOfTheMonth(2),
                    getTomorrowMonth(2)
                )
                adapter.addItemList(item1)
                adapter.addItemList(item2)
                adapter.addItemList(item3)
                adapter.addItemList(item4)
                adapter.addItemList(item5)
                adapter.addItemList(item6)
                adapter.addItemList(item7)
                adapter.addItemList(item8)
                adapter.addItemList(item2)
                adapter.addItemList(item3)
                adapter.addItemList(item7)
            }

    }
    fun dayOfWeek (): String {
        var dayOfWeek = SimpleDateFormat("EEEE", Locale("Ru", "RU")).format(Date())
        return dayOfWeek
    }
    fun getDayOfMonth(): String{
        var getDayOfMonth = SimpleDateFormat("dd", Locale("Ru", "RU")).format(Date())
        return getDayOfMonth
    }
    fun getMonth(): String{
        var getMonth = SimpleDateFormat("MMMM", Locale("Ru", "RU")).format(Date())
        return getMonth
    }
    @SuppressLint("NewApi")
    fun getTomorrow (nextDay:Int): String {
        var parsedDate = LocalDate.now().plusDays(nextDay.toLong())
        var getTomorrow = parsedDate.dayOfWeek.toString().lowercase()
        val dayOfWeekArrayEnglish =
            arrayOf("monday", "tuesday", "wednesday", "thursday", "friday", "saturday","sunday")

        val dayOfWeekArrayRussian =
            arrayOf("понедельник","вторник", "среда", "четверг", "пятница", "суббота", "воскресенье")

        for (index in dayOfWeekArrayEnglish.indices) {
            if (getTomorrow == dayOfWeekArrayEnglish[index]) {
                getTomorrow = dayOfWeekArrayRussian[index]
            }
        }
        return getTomorrow
    }
    @SuppressLint("NewApi")
    fun getTomorrowDateOfTheMonth(nextDay:Int): String{
        var parsedDate = LocalDate.now().plusDays(nextDay.toLong())
        var getTomorrowDateOfTheMonth = parsedDate.dayOfMonth.toString()
        return getTomorrowDateOfTheMonth
    }
    @SuppressLint("NewApi")
    fun getTomorrowMonth(nextDay:Int): String{
        var parsedDate = LocalDate.now().plusDays(nextDay.toLong())
        var getTomorrowMonth = parsedDate.month.toString().lowercase()
        val monthArrayEnglish =
            arrayOf("january","february", "march", "april", "may", "june", "july", "august",
                "september", "october", "november", "december",)
        val monthArrayRussian =
            arrayOf("января","февраля", "марта", "апреля", "мая", "июня", "июля", "августа",
                "сентября", "октября", "ноября", "декабря",)

        for (index in monthArrayEnglish.indices) {
            if (getTomorrowMonth == monthArrayEnglish[index]) {
                getTomorrowMonth = monthArrayRussian[index]
            }
            }
            return getTomorrowMonth
        }
//    fun now(): String {
//        return SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Date())
//    }


    companion object {

        @JvmStatic
        fun newInstance() = BlockFragment()
    }
}