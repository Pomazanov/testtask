package com.example.testtask.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.testtask.databinding.ListItemBinding
import com.example.testtask.model.TaskCell

class ListItemHolder(item: View): RecyclerView.ViewHolder(item){
    val bindingClass = ListItemBinding.bind(item)
    fun bind(info: TaskCell) = with(bindingClass){
        tvTrainingTimeStart.text = info.trainingTimeStart
        tvTrainingTimeEnd.text = info.trainingTimeEnd
        tvTraningName.text = info.trainingName
        tvInfoPerson.text = info.nameOrNumberVisitors
        tvLocation.text = info.location
        imBackgroundItem.setImageResource(info.colorStatus)
        imPerson.setImageResource(info.moreThanOne)
    }
}