package com.example.testtask.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.testtask.R
import com.example.testtask.model.*

class MultiTypeRecyclerViewAdapter():RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val TYPE_A = 1
    val TYPE_B = 2
    val itemList = ArrayList<ViewItem>()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType == TYPE_A) {
            HeaderHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_header,parent,false))
        } else {
            ListItemHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item,parent,false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        return if (itemList[position].viewType == TYPE_A) {
            val header = holder as HeaderHolder
            header.bind((itemList[position] as DateCell))
        } else {
            (holder as ListItemHolder).bind((itemList[position] as TaskCell))
        }
    }


    fun addItemList(view: ViewItem) {
        itemList.add(view)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return itemList[position].viewType
    }
    override fun getItemCount(): Int = itemList.size
}