package com.example.testtask.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.testtask.databinding.ListHeaderBinding
import com.example.testtask.model.DateCell

class HeaderHolder(header: View) : RecyclerView.ViewHolder(header) {
    val bindingClass = ListHeaderBinding.bind(header)
    fun bind(info: DateCell) = with(bindingClass) {
        tvData.text = "${info.dayOfWeek}, ${info.dateOfTheMonth} ${info.month}"

    }
}

