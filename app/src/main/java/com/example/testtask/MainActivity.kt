package com.example.testtask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.testtask.databinding.ActivityMainBinding
import com.example.testtask.fragments.MainFragment

class MainActivity : AppCompatActivity() {
    lateinit var bindingClass: ActivityMainBinding
//    private val adapter = ItemAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingClass = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bindingClass.root)

        supportFragmentManager
            .beginTransaction().replace(R.id.placeHolderDataInfo, MainFragment.newInstance())
            .commit()


        navigationBarBottom()

    }

    fun navigationBarBottom (){
        bindingClass.bNavigationBottom.selectedItemId = R.id.calendar_activites
        bindingClass.bNavigationBottom.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.calendar_activites -> {
                    Toast.makeText(this, R.string.calendar_activites, Toast.LENGTH_SHORT).show()
                }
                R.id.applications -> {
                    Toast.makeText(this, R.string.applications, Toast.LENGTH_SHORT).show()
                }
                R.id.adding -> {
                    Toast.makeText(this, R.string.adding, Toast.LENGTH_SHORT).show()
                }
                R.id.chat -> {
                    Toast.makeText(this, R.string.chat, Toast.LENGTH_SHORT).show()
                }
                R.id.more -> {
                    Toast.makeText(this, R.string.more, Toast.LENGTH_SHORT).show()
                }

            }
            true
        }
    }

}