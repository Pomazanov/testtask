package com.example.testtask.model

interface ViewItem{
    val viewType: Int
}

data class DateCell(
    override val viewType: Int,
    val dayOfWeek: String,
    val dateOfTheMonth: String,
    val month: String
) : ViewItem

data class TaskCell(
    override val viewType: Int,
    val trainingTimeStart: String,
    val trainingTimeEnd: String,
    val trainingName: String,
    val nameOrNumberVisitors: String,
    val location: String,
    val colorStatus: Int,
    val moreThanOne: Int
) : ViewItem

